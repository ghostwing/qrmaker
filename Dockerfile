FROM python:3.12.3-alpine AS qrmaker
USER root

RUN apk update && apk add --no-cache \
    librsvg-dev \
    font-liberation

RUN mkdir -p /src
WORKDIR /src

COPY requirements.txt /usr/local/pip-requirements/
RUN pip3 install \
    --quiet \
    --no-binary :none: \
    -r /usr/local/pip-requirements/requirements.txt

COPY templates/* ./templates/
COPY qrmaker.py ./

# Add "--debug" at the end of the command parameters if you need some error log output in your navigator
CMD ["python3", "/usr/local/bin/flask", "--app", "qrmaker", "run", "--host", "0.0.0.0", "--port", "80"]
