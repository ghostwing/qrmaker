import json
import logging
import subprocess
import tempfile
from datetime import datetime
from pathlib import Path

from flask import Flask, make_response, render_template, request, send_file
from wtforms import Form, RadioField, SelectField, StringField

from qrbill.bill import QRBill

app = Flask(__name__)
logging.basicConfig(filename=Path(__file__).parent / 'billing.log', level=logging.INFO)


class BillForm(Form):
    # We are very permissive at form level, more strict validation happens in qrbill.
    account = StringField('Compte IBAN')
    currency = SelectField('Monnaie', choices=[('CHF', 'CHF'), ('EUR', 'EUR')])
    amount = StringField('Montant')
    reference = StringField('Référence')
    creditor_name = StringField('Nom du bénéficiaire')
    creditor_street = StringField('Rue')
    creditor_housenum = StringField('Numéro')
    creditor_npa = StringField('NPA')
    creditor_city = StringField('Localité')
    debtor_name = StringField('Nom')
    debtor_street = StringField('Rue')
    debtor_housenum = StringField('Numéro')
    debtor_npa = StringField('NPA')
    debtor_city = StringField('Localité')
    img_format = RadioField('Format d’image', choices=[('svg', 'SVG'), ('png', 'PNG')])


COOKIE_SAVED_FIELDS = [
    'account', 'creditor_name', 'creditor_street', 'creditor_housenum',
    'creditor_npa', 'creditor_city',
]

@app.route('/', methods=['POST', 'GET'])
def create_bill():
    error = ''
    form = BillForm(request.form)
    if request.method == 'POST' and form.validate():
        debtor = {
            'name': form.debtor_name.data,
            'street': form.debtor_street.data, 'house_num': form.debtor_housenum.data,
            'pcode': form.debtor_npa.data, 'city': form.debtor_city.data,
        } if form.debtor_name.data else None

        try:
            bill = QRBill(
                language='fr',
                account=form.account.data,
                currency=form.currency.data,
                amount=form.amount.data if len(form.amount.data) else None,
                ref_number=form.reference.data,
                creditor={
                    'name': form.creditor_name.data,
                    'street': form.creditor_street.data,
                    'house_num': form.creditor_housenum.data,
                    'pcode': form.creditor_npa.data,
                    'city': form.creditor_city.data,
                },
                debtor=debtor,
            )
        except Exception as err:
            error = str(err)
        else:
            
            with tempfile.NamedTemporaryFile() as tmp, tempfile.NamedTemporaryFile() as tmp2:
                bill.as_svg(tmp.name)
                tmp_file, ext, mime = tmp, 'svg', 'image/svg+xml'
                if form.img_format.data == 'png':
                    subprocess.run(['rsvg-convert', '-z', '3', tmp.name, '-o', tmp2.name])
                    tmp_file, ext, mime = tmp2, 'png', 'image/png'
                resp = make_response(send_file(
                    tmp_file.name,
                    mimetype=mime,
                    as_attachment=True,
                    download_name=f"facture_{datetime.now().strftime('%Y_%m_%d_%H_%M')}.{ext}"
                ))
                saved_data = {field_name: getattr(form, field_name).data for field_name in COOKIE_SAVED_FIELDS}
                resp.set_cookie('billdata', json.dumps(saved_data), max_age=60 * 60 * 24 * 90)  # 3 months
                app.logger.info(
                    f'Bill produced (account: {form.account.data}, amount: {form.amount.data}, '
                    f'creditor name: {form.creditor_name.data})'
                )
                return resp
    elif request.method == 'GET':
        saved_data = json.loads(request.cookies.get('billdata') or '{}')
        for field_name, value in saved_data.items():
            getattr(form, field_name).data = saved_data[field_name]
    return render_template('qrform.html', form=form, error=error)
